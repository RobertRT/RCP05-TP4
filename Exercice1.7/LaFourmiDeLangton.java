package TP4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LaFourmiDeLangton extends JFrame implements ActionListener {
	private int dimension=100;
	private Container panneau;
	JButton next=new JButton("Next");
	int nbretour=0;
	JLabel label1=new JLabel("Nombre de tour= 0");
	JTextField[][] plateau= new JTextField [dimension][dimension];
	int xfourmi;
	int yfourmi;
	int position;
	String affichage;



	LaFourmiDeLangton(){
		setSize(500,500);
		setLocation(300,200);
		setTitle("� La Fourmi de Langton");

		panneau=getContentPane();
		panneau.add(label1,BorderLayout.NORTH);
		panneau.add(next,BorderLayout.SOUTH);
		panneau.add(this.creerPlateau(),BorderLayout.CENTER);
		initFourmi();

		next.addActionListener(this);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		automatic();

	}
	public JPanel creerPlateau(){

		JPanel p=new JPanel();
		p.setLayout(new GridLayout(dimension,dimension));
		for(int i=0;i<100;i++){
			for(int j=0;j<100;j++){
				plateau[i][j]=new JTextField();
				p.add(plateau[i][j]);
				plateau[i][j].setBackground(Color.WHITE);

			}

		}


		return p;

	}

	void initFourmi(){
		xfourmi=50;
		yfourmi=50;
		position=0;
		affichage=position+"�";
		plateau[xfourmi][yfourmi].setText(affichage);
	}
	void automatic() {
		while(xfourmi>0||xfourmi<99||yfourmi>0||yfourmi<99){
			next();
			try {
				Thread.sleep(2);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

		}
	}

	void next(){
		if(plateau[xfourmi][yfourmi].getBackground()==Color.WHITE){
			if(position==0){
				position=270;
				plateau[xfourmi][yfourmi].setBackground(Color.BLACK);


			} 
			else{
				position-=90;
				plateau[xfourmi][yfourmi].setBackground(Color.BLACK);
				affichage=position+"�";

			}
		}
		else{
			if(position==270){
				position=0;
				plateau[xfourmi][yfourmi].setBackground(Color.WHITE);


			}
			else{
				position+=90;
				plateau[xfourmi][yfourmi].setBackground(Color.WHITE);

			}


		}
		plateau[xfourmi][yfourmi].setText("");
		switch(position){
		case 0:
			xfourmi--;
			break;
		case 90:
			yfourmi++;
			break;
		case 180:
			xfourmi++;
			break;
		case 270:
			yfourmi--;
		}
		affichage=position+"�";
		plateau[xfourmi][yfourmi].setText(affichage);
		nbretour++;
		label1.setText("Nombre de tour= "+ nbretour );


	}
	public void actionPerformed(ActionEvent e){


	}

}



